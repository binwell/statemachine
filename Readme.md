﻿# Binwell StateMachine for Xamarin.Forms 

Demo of using finite state machine (FSM) for making complex animation

Here is an article with detailed description in Russian: [https://medium.com/binwell/9925d928eb92](https://medium.com/binwell/9925d928eb92)

[![Watch Youtube demo](https://img.youtube.com/vi/8qDfWoq-zpg/maxresdefault.jpg)](https://www.youtube.com/watch?v=8qDfWoq-zpg)